/*
    Задание №4
*/
var fet = require('node-fetch'); // Модуль чтения  HTML страниц 
var file = require('fs'); // Модуль работы с файловой системой
fet('https://ru.wikipedia.org/wiki/TypeScript') // Считывание данных
    .then(function (res) { return res.text(); })
    .then(function (body) {
    file.writeFileSync('control.txt', body);
});
var data = file.readFileSync('control.txt');
data = data.toString();
data = data.split(/<[^>]+>/g);
var text = "";
var count = 0;
data.forEach(function (word) {
    word = word.trim();
    if (word.length)
        text = text + " " + word;
});
var new_text = text.split(' ');
new_text.forEach(function (piece) {
    if (piece.length > 5)
        count++;
});
console.log("%d слов, в которых больше 5 букв", count);

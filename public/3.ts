/*
    Задание №3
*/

const file = require('fs');
let data = file.readFileSync('control.txt');
data = data.toString();
data = data.split(/<[^>]+>/g);
let text = "";
let count = 0;

data.forEach(function (word) {
    word = word.trim();

    if (word.length)
        text = text + " " + word;
});

var new_text = text.split(' ');

new_text.forEach(function(piece) {
    if (piece.length > 5)
        count ++;
});

console.log("%d слов, в которых больше 5 букв", count);
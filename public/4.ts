/*
    Задание №4
*/

const fet = require('node-fetch');// Модуль чтения  HTML страниц 
const file = require('fs');// Модуль работы с файловой системой

fet('https://ru.wikipedia.org/wiki/TypeScript') // Считывание данных
    .then(res => res.text())
    .then(body => { 
        file.writeFileSync('control.txt', body);
});


let data = file.readFileSync('control.txt');
data = data.toString();
data = data.split(/<[^>]+>/g);
let text = "";
let count = 0;

data.forEach(function (word) {
    word = word.trim();

    if (word.length)
        text = text + " " + word;
});

var new_text = text.split(' ');

new_text.forEach(function(piece) {
    if (piece.length > 5)
        count ++;
});

console.log("%d слов, в которых больше 5 букв", count);